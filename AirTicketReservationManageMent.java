package com.ilp.tcs.airTicketReservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AirTicketReservationManageMent {
	public static void main(String[] args) {
		AirTicketReservation atr = new AirTicketReservation();
		//adding customer
		atr.addCustomer(100, "Avik", "3 No.", 9875666897L);
		atr.addCustomer(101, "Bibek", "4 No.", 9775662891L);
		atr.addCustomer(102, "Soumik", "5 No.", 9870178895L);
		atr.addCustomer(103, "Amitabha", "6 No.", 9874706800L);
		//creating date objects for our use
		Date d1 = new Date("7/5/2016");
		Date d2 = new Date("8/5/2016");
		Date d3 = new Date("9/5/2016");
		Date d4 = new Date("10/5/2016");
		//adding flights
		//atr.addFlight(flightId, source, destination, cost, totalSeats, seatsRemaining, date)
		atr.addFlight(450, "kolkata", "hyderabad", 7000, 100, 80, d1);
		atr.addFlight(451, "chennai", "mumbai", 5000, 150, 49, d2);
		atr.addFlight(452, "goa", "bangalore", 5080, 170, 50, d3);
		atr.addFlight(453, "kolkata", "trivandrum", 12000, 40, 40, d4);

		try{
			Flight desiredFlight = atr.getFlight("kolkata", "hyderabad");
			System.out.println("Flight is available. Flight details are : ");
			System.out.println("Cost : " + desiredFlight.getCost() + " Seats available : " + desiredFlight.getSeatsRemaining() + "Date : " + desiredFlight.getDate() + "\n");
		} catch (FlightDoesNotExist e) {
			System.out.println("Ops.. \n");
		}

		try{
			int availableSeats = atr.getAvailableSeats(450, d1);
			System.out.println("Reamining seats are : " + availableSeats + "\n");
		} catch (FlightAlreadyFullException e) {
			System.out.println("Ops... \n");
		}

		try{
			Ticket t1 = atr.bookTicket(451, 101, 800, d2, 5);
			System.out.println("Your ticket has been booked");
			System.out.println("ticket id : " + t1.getTicketId() + "total cost : " + t1.getTotalCost() + "\n");

			Ticket t2 = atr.bookTicket(452, 101, 801, d3, 4);
			System.out.println("Your ticket has been booked");
			System.out.println("ticket id : " + t2.getTicketId() + "total cost : " + t2.getTotalCost() + "\n");

		} catch (FlightNotFoundException e) {
			System.out.println("Ops... \n");
		} catch (FlightAlreadyFullException e) {
			System.out.println("Ops... \n");
		} catch (LimitedSeatsException e) {
			System.out.println("Ops... \n");
		}

		//cancelling ticket
		atr.cancelTicket(800, 101);


		//here I am going to show only ticket id and remaining seat of that flight
		HashMap<Integer, Flight> allFlight = atr.getAllFlightMap();
		if(allFlight.size() != 0){
			System.out.println("Flight Id : Remaining Seats");
			for(Map.Entry<Integer, Flight> e : allFlight.entrySet()){
				System.out.println(e.getKey() + " : " + e.getValue().getSeatsRemaining());
			}
		} else {
			System.out.println("no flights available");
		}
		
		//get sorted ticket details
		ArrayList<Ticket> sortedTickets = atr.getTicket(101);
		for(Ticket t : sortedTickets){
			System.out.println("Ticket Id " + t.getTicketId() + " : " + " Total Cost " + t.getTotalCost());
		}
	}

}
