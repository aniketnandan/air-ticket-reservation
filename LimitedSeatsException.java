package com.ilp.tcs.airTicketReservation;

public class LimitedSeatsException extends Exception {
	public LimitedSeatsException(int flightId, int availableSeats){
		System.out.println("Flight with id : " + flightId + " has only " + availableSeats + " seats available");
	}
}
