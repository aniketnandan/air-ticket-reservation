package com.ilp.tcs.airTicketReservation;
import java.util.ArrayList;

public class Customer {
	private int customerId;
	private String customerName;
	private String address;
	private long phoneNo;
	private ArrayList<Ticket> tickets;
	
	public Customer(int customerId, String customerName, String address, long phoneNo) {
		tickets = new ArrayList<Ticket>();
		this.customerId = customerId;
		this.customerName = customerName;
		this.address = address;
		this.phoneNo = phoneNo;
	}

	public Ticket addTicket(int ticketId, Date bookingDate, Date journeyDate, int noOfPerson, double totalCost, Flight plane){
		boolean ticketBooked = false;
		for(Ticket t : tickets){
			if(t.getTicketId() == ticketId){
				ticketBooked = true;
				break;
			}
		}
		if(!ticketBooked){
			Ticket t = new Ticket(ticketId, bookingDate, journeyDate, noOfPerson, totalCost, plane);
			tickets.add(t);
			return t;	
		} else {
			System.out.println("Sorry, this ticket is already booked");
		}
		return null;
		
	}
	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}

	public ArrayList<Ticket> getTickets() {
		return tickets;
	}
}
