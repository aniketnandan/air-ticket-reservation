package com.ilp.tcs.airTicketReservation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

public class AirTicketReservation {
	private ArrayList<Flight> flights;
	private ArrayList<Customer> customers;

	public AirTicketReservation(){
		flights = new ArrayList<Flight>();
		customers = new ArrayList<Customer>();
	}

	public void addCustomer(int customerId, String customerName, String address, long phoneNo) {
		boolean customerAlreadyExist = false;
		for(Customer c : customers){
			if(c != null){
				if(c.getCustomerId() == customerId){
					customerAlreadyExist = true;
					break;
				}
			}
		}
		if(!customerAlreadyExist){
			customers.add(new Customer(customerId, customerName, address, phoneNo));
			System.out.println("customer Succeessfully added\n");
		} else {
			System.out.println("customer with id " + customerId + "is already present\n");
		}
	}

	public void addFlight(int flightId, String source, String destination, double cost,	int totalSeats, int seatsRemaining, Date date){
		boolean flightAlreadyExist = false;
		for(Flight f : flights){
			if(f != null) {
				if(f.getFlightId() == flightId){
					flightAlreadyExist = true;
					break;
				}
			}
		}
		if(!flightAlreadyExist){
			flights.add(new Flight(flightId, source, destination, cost, totalSeats, seatsRemaining, date));
			System.out.println("Flight is successfully added\n");
		} else {
			System.out.println("Flight with id : " + flightId + " is already exist\n");
		}
	}

	public Flight getFlight(String source, String destination) throws FlightDoesNotExist {
		Flight choosedFlight = null;
		boolean flightExist = false;
		for(Flight f : flights){
			if((f.getSource().equalsIgnoreCase(source)) && (f.getDestination().equalsIgnoreCase(destination))){
				choosedFlight = f;
				flightExist = true;
				break;
			}
		}

		if(flightExist) {
			return choosedFlight;
		} else {
			throw new FlightDoesNotExist(source, destination);
		}
	}

	public int getAvailableSeats(int flightId, Date date) throws FlightAlreadyFullException {
		int availableSeats = 0;
		for(Flight f : flights){
			if((f.getDate().equalsIgnoreCase(date.getDate())) && (f.getFlightId() == flightId)){
				availableSeats = f.getSeatsRemaining();
				break;
			}
		}
		if(availableSeats > 0){
			return availableSeats;
		} else {
			throw new FlightAlreadyFullException(flightId);
		}
	}

	public Ticket bookTicket(int flightId, int custId, int ticketId, Date dateOfJourney, int noOfPerson) throws FlightAlreadyFullException, FlightNotFoundException, LimitedSeatsException{
		boolean flightExist = false;
		Flight selectedFlight = null;
		for(Flight f : flights){
			if(f.getFlightId() == flightId){
				selectedFlight = f;
				flightExist = true;
				break;
			}
		}
		if(flightExist) {
			if(selectedFlight.getSeatsRemaining() == 0){
				throw new FlightAlreadyFullException(flightId);
			} else if(selectedFlight.getSeatsRemaining() < noOfPerson){
				throw new LimitedSeatsException(flightId, selectedFlight.getSeatsRemaining());
			} else {
				boolean custAvailable = false;
				Customer selectedCustomer = null;
				for(Customer c : customers){
					if(c.getCustomerId() == custId){
						selectedCustomer = c;
						custAvailable = true;
					}
				}
				if(custAvailable){
					Ticket t = selectedCustomer.addTicket(ticketId, new Date("2/5/2016"), dateOfJourney, noOfPerson, (noOfPerson * selectedFlight.getCost()), selectedFlight);
					selectedFlight.setSeatsRemaining(selectedFlight.getSeatsRemaining() - noOfPerson);
					return t;
				} else {
					System.out.println("This customer with " + custId + " is not exist");
				}
			}
		} else {
			throw new FlightNotFoundException(flightId);
		}
		return null;
	}

	public void cancelTicket(int ticketId, int custId){
		Customer selectedCustomer = null;
		boolean customerExist = false;
		for(Customer c : customers){
			if(c.getCustomerId() == custId){
				selectedCustomer = c;
				customerExist = true;
				break;
			}
		}
		if(customerExist) {
			boolean ticketFound = false;
			Ticket ticket = null;
			for(Ticket t : selectedCustomer.getTickets()){
				if(t.getTicketId() == ticketId){
					ticketFound = true;
					ticket = t;
					break;
				}
			}
			if(ticketFound){
				Flight recentFlight = ticket.getPlane(); 
				recentFlight.setSeatsRemaining(recentFlight.getSeatsRemaining() + ticket.getNoOfPerson());
				ticket.setPlane(null);
				System.out.println("your ticket has been cancelled\n");
			} else {
				System.out.println("Ticket has already been cancelled.\n");
			}
		} else {
			System.out.println("you have entered wrong customer id.\n");
		}
	}

	public ArrayList<Ticket> getTicket(int customerId) {
		ArrayList<Ticket> ticketList = new ArrayList<Ticket>();
		for(Customer c : customers){
			if(c.getCustomerId() == customerId){
				for(Ticket t : c.getTickets()){
					ticketList.add(t);
				}
			}
		}
		
		Ticket temp = null;
		for(int i = 0; i < (ticketList.size() - 1); i++){
			for(int j = i+1; j < ticketList.size(); j++){
				if(ticketList.get(i).getTotalCost() > ticketList.get(j).getTotalCost()){
					temp =  ticketList.get(i);
					ticketList.add(i, ticketList.get(j));
					ticketList.add(j, temp);
				}
			}
		}
		return ticketList;
	}

	public HashMap<Integer, Flight> getAllFlightMap(){
		HashMap<Integer, Flight> hm = new HashMap<Integer, Flight>();
		for(Flight f : flights){
			if(f.getSeatsRemaining() != 0){
				hm.put(f.getFlightId(), f);
			} else{
				continue;
			}
		}
		return hm;
	}

}
