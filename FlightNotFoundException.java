package com.ilp.tcs.airTicketReservation;

public class FlightNotFoundException extends Exception {
	public FlightNotFoundException(int flightId){
		System.out.println("Flight with id : " + flightId + " could not be found");
	}
}
