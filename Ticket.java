package com.ilp.tcs.airTicketReservation;

public class Ticket {
	private int ticketId;
	private Date bookingDate;
	private Date journeyDate;
	private int noOfPerson;
	private double totalCost;
	private Flight plane;
	
	public Ticket(int ticketId, Date bookingDate, Date journeyDate,	int noOfPerson, double totalCost, Flight plane) {
		this.ticketId = ticketId;
		this.bookingDate = bookingDate;
		this.journeyDate = journeyDate;
		this.noOfPerson = noOfPerson;
		this.totalCost = totalCost;
		this.plane = plane;
	}
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public Date getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}
	public Date getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(Date journeyDate) {
		this.journeyDate = journeyDate;
	}
	public int getNoOfPerson() {
		return noOfPerson;
	}
	public void setNoOfPerson(int noOfPerson) {
		this.noOfPerson = noOfPerson;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public Flight getPlane() {
		return plane;
	}
	public void setPlane(Flight plane) {
		this.plane = plane;
	}
}
