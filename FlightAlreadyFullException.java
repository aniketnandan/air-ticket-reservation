package com.ilp.tcs.airTicketReservation;

public class FlightAlreadyFullException extends Exception {
	public FlightAlreadyFullException(int flightId){
		System.out.println("The flight with id : " + flightId + " is already full");
	}
}
