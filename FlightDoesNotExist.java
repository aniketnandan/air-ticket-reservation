package com.ilp.tcs.airTicketReservation;

public class FlightDoesNotExist extends Exception{
	public FlightDoesNotExist(String source, String destination){
		System.out.println("Flight with destination : " + destination + " and source : " + source + " does not exist");
	}
}
