package com.ilp.tcs.airTicketReservation;

public class Flight {
	private int flightId;
	private String source;
	private String destination;
	private double cost;
	private int totalSeats;
	private int seatsRemaining;
	private Date date;
	
	public Flight(int flightId, String source, String destination, double cost,	int totalSeats, int seatsRemaining, Date date) {
		this.flightId = flightId;
		this.source = source;
		this.destination = destination;
		this.cost = cost;
		this.totalSeats = totalSeats;
		this.seatsRemaining = seatsRemaining;
		this.date = date;
	}
	public int getFlightId() {
		return flightId;
	}
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getTotalSeats() {
		return totalSeats;
	}
	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}
	public int getSeatsRemaining() {
		return seatsRemaining;
	}
	public void setSeatsRemaining(int seatsRemaining) {
		this.seatsRemaining = seatsRemaining;
	}
	public String getDate() {
		return date.getDate();
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
